import Link from 'next/link'
import { ReactiveBase, CategorySearch } from '@appbaseio/reactivesearch'
import { Container, Row, Col } from 'reactstrap'
import MediaQuery from 'react-responsive'
import Layout from '../components/Layout'
import settings from '../utils/credentials'
const forwordButton = (
    <svg width="13px" height="22px" viewBox="0 0 13 22" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="Search-results,-Company-detail-and-category-detail" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" opacity="0.703962054">
            <g id="Company-Detail" transform="translate(-140.000000, -113.000000)" fill="#FF730D">
                <polygon id="Fill-1" points="152.922 131.903641 144.88339 123.875941 152.922 115.846059 150.475941 113.4 140 123.875941 150.475941 134.351882"></polygon>
            </g>
        </g>
    </svg>
)
const agreements = {
    "Mergers and Acquisitions": ["Advisory Agreement", "Agreement and Plan of Merger", "Asset Purchase Agreement", "Assignment and Assumption","Business Separation Agreement","Closing Agreement","Contribution Agreement", "Contribution and Distribution Agreement", "Earn-Out Agreement", "Employee Benefits Agreement","Employee Matters Agreement","Equity Exchange Agreement","Escrow Agreement","Funding Agreement","Investor Rights Agreement","IP Agreement  Management Services Agreement","Membership Interest Purchase Agreement","Misc M&A Agreement","Option Agreement","Plan of Reorganization","Purchase Agreement","Purchase and Sale Agreement","Restrictive Covenant Agreement","Stock Purchase Agreement","Subscription Agreement","Transaction Agreement","Transition Services Agreement"],
    "Incentive & Benefit Plans": ["Administrative Plan","Automobile Plan","Award Program","Benefit Equalization Plan","Benefit Plan","Bonus Plan","Change in Control Plan","Charitable Award Program","Clawback Plan","Compensation Plan","Deferred Plan","Director Plan","Disability Plan","Employee Protection Plan","Excess Plan","Financial Counseling Plan","Grantor Trust","Health Plan","Incentive Plan","Investment Plan","Life Insurance Plan","Management Continuity Agreement","Option Plan","Pension Plan","Performance Plan","Phantom Equity Plan","Relocation Plan","Retention Plan","Retirement Plan","Savings Plan","Severance Plan","Stock Option Agreement","Stock Plan","Supplemental Plan","Termination Plan"],
    "Employment and Consulting": ["Bonus Agreement","Change of Control Agreement","Confidentiality and Inventions Assignment Agreement","Confidentiality and Non-Competition Agreement","Consulting Services Agreement","Deferred Compensation Agreement","Employment Agreement","Employment Offer Letter","Expatriate Agreement","Letter Agreement","Misc Emloyment and Consulting Agreements","Non-Competition and Non-Solicitation Agreement","Release Agreement","Relocation Agreement","Resignation Agreement","Restrictive Covenant Agreement","Retention Agreement","Retirement Agreement","Separation Agreement","Severance Agreement","Termination Agreement","Transition Agreements"],
    "Entity Governance": ["Advisory Board Member Agreement","Articles of Incorporation","Board Member Agreement","Bylaws","Certificate of Conversion","Code of Conduct Contribution Agreement","Corporate Governance Principles","Equity Ownership Policy","Indemnification Agreement","Instrument of Admission","Joint Venture Agreement","Limited Partnership Agreement","Litigation Management Agreement","LLC Operating Agreement","Lock Up Agreement","Partnership Agreement","Plan of Dissolution","Registration Rights Agreement","Shareholders Agreement","Stock Restriction Agreement","Stockholders and Registration Rights Agreement","Support Agreement","Voting Agreement","Voting and Lock-Up Agreement"],
    "Credit and Finance": ["Commitment Letter","Convertible Note","Credit Agreement","Guaranty","Indenture","Intercreditor Agreement","Letter of Credit","Line of Credit Agreement","Loan Agreement","Note Purchase Agreement","Other Credit and Finance Agreements","Pledge Agreement","Promissory Note","Revolving Credit Agreement","Security Agreement","Subordination Agreement","Warrant"],
    "Licensing / Technology": ["Collaboration Agreement","Copromotion Agreement","Development Agreement","Exclusivity and IP Agreement","Joint Development Agreement","License Agreement Online Advertising Agreement","Patent License Agreement","Patent Ownership Agreement","Software License Agreement","Sponsored Research Agreement","Technology Transfer Agreement"],
    "Commercial (Goods/Services)": ["Aviation Agreement","Business Cooperation Agreement","Construction Agreement","Cost Plus Agreement","Development Agreement","Distribution Agreement","Equipment Lease","Manufacturing Agreement","Master Agreement","Other Services Agreement","Power Supply Agreement","Purchase Agreement (Goods)","Reimbursement Agreement","Sale Agreement (Product/Service)","Sales Representative Agreement","Services Agreement, Supply Agreement","Tech Services Agreement"],
    "Misc Agreements": ["Cooperation Agreement","Corporate Integrity Agreement","Credit Card Issuer","Employee Matters Agreement","Energy and Power","Enterprise Funding Agreement","Investor Agreement","Judgment Sharing Agreement","Misc","Pharma Agreements","Settlement Agreement","Zero Emission Vehicle Credit Agreement"],
    "Real Estate": ["Data Center Lease","Environmental Agreement","Lease Agreement","Real Estate License","Real Estate Purchase Agreement","Sublease"],
    "Tax": ["Tax Allocation Agreement","Tax Matters Agreement","Tax Receivable Agreement","Tax Sharing Agreement"]
}
const PopularCategories = () => {
    return (
        <Container className="catagories-list">
            <Row>
                <Col>
                    <ul>
                        <li>Mergers and Acquisitions</li>
                        {
                            agreements["Mergers and Acquisitions"].map((e, i) => 
                                <li key={i}>
                                    <Link href={encodeURI(`search?Contract Type=["`+e+`"]`)}><a>{e}</a></Link>
                                </li>
                            )
                        }

                        <li>Entity Governance</li>
                        {
                            agreements["Entity Governance"].map((e, i) => 
                                <li key={i}>
                                    <Link href={encodeURI(`search?Contract Type=["`+e+`"]`)}><a>{e}</a></Link>
                                </li>
                            )
                        }

                        <li>Licensing / Technology</li>
                        {
                            agreements["Licensing / Technology"].map((e, i) => 
                                <li key={i}>
                                    <Link href={encodeURI(`search?Contract Type=["`+e+`"]`)}><a>{e}</a></Link>
                                </li>
                            )
                        }              
                    </ul>
                </Col>
                <Col>
                    <ul>
                        <li>Commercial (Goods/Services)</li>
                        {
                            agreements["Commercial (Goods/Services)"].map((e, i) => 
                                <li key={i}>
                                    <Link href={encodeURI(`search?Contract Type=["`+e+`"]`)}><a>{e}</a></Link>
                                </li>
                            )
                        }
                        <li>Incentive & Benefit Plans</li>
                        {
                            agreements["Incentive & Benefit Plans"].map((e, i) => 
                                <li key={i}>
                                    <Link href={encodeURI(`search?Contract Type=["`+e+`"]`)}><a>{e}</a></Link>
                                </li>
                            )
                        }

                        <li>Tax</li>
                        {
                            agreements["Tax"].map((e, i) => 
                                <li key={i}>
                                    <Link href={encodeURI(`search?Contract Type=["`+e+`"]`)}><a>{e}</a></Link>
                                </li>
                            )
                        }
                    </ul>
                </Col>
                <Col>
                    <ul>
                        <li>Misc Agreements</li>
                        {
                            agreements["Misc Agreements"].map((e, i) => 
                                <li key={i}>
                                    <Link href={encodeURI(`search?Contract Type=["`+e+`"]`)}><a>{e}</a></Link>
                                </li>
                            )
                        }

                        <li>Employment and Consulting</li>
                        {
                            agreements["Employment and Consulting"].map((e, i) => 
                                <li key={i}>
                                    <Link href={encodeURI(`search?Contract Type=["`+e+`"]`)}><a>{e}</a></Link>
                                </li>
                            )
                        }

                        <li>Real Estate</li>
                        {
                            agreements["Real Estate"].map((e, i) => 
                                <li key={i}>
                                    <Link href={encodeURI(`search?Contract Type=["`+e+`"]`)}><a>{e}</a></Link>
                                </li>
                            )
                        }

                        <li>Credit and Finance</li>
                        {
                            agreements["Credit and Finance"].map((e, i) => 
                                <li key={i}>
                                    <Link href={encodeURI(`search?Contract Type=["`+e+`"]`)}><a>{e}</a></Link>
                                </li>
                            )
                        }
                    </ul>
                </Col>
            </Row>
        </Container>
    )
}
const Home = () => (
    <Layout 
        title="Kfetch - Thousands of Curated Contracts"
        description="contract management"
        ogType=""
        ogUrl="https://www.kfetch.com"
        ogImage=""
    >
        <ReactiveBase {...settings}>
            <div className="landing-content">
                <div className="s-row" style={{ minHeight: 3571 }}>
                    <div className="hero">
                        <MediaQuery query="(min-device-width: 1224px)">
                            <img style={{ position: 'absolute', right: 0, width: 810 }} src="https://d127eqd96jr9aw.cloudfront.net/images/landing-img.jpg" />
                        </MediaQuery>
                        <div className="hero-text">
                            <h4 style={{ position: 'relative', float: 'left' }}>Your contract search ends here…</h4>
                            <h6 style={{ position: 'relative', float: 'left' }}>1.4M+ Contracts available</h6>
                            {/* CategorySearch  */}

                            <div className="hero-button-container">
                                <a href="/companies">
                                    <div className="hero-button">
                                        <p className="category-box-title">Browse by company</p>
                                        <p className="category-box">View all contracts by a company
                                        in alphabetical order</p>
                                        <MediaQuery query="(min-device-width: 1224px)">
                                            <div>{forwordButton}</div>
                                        </MediaQuery>
                                    </div>
                                </a>
                                <a href="/categories">
                                    <div className="hero-button">
                                        <p className="category-box-title">Browse by category</p>
                                        <p className="category-box">View all contracts by a category name</p>
                                        <MediaQuery query="(min-device-width: 1224px)">
                                            <div>{forwordButton}</div>
                                        </MediaQuery>
                                    </div>
                                </a>
                            </div>
                            <div className="popular-cat">
                                <label>Popular Categories</label>
                                <PopularCategories />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ReactiveBase>
    </Layout>
)

export default Home