import Head from 'next/head'
import Navigation from './Navigation'

const Header = () => (
    <div>
        <Head>
            <meta 
                name="description"
                content="Kfetch home page"
            />
            <title>Kfetch: Home Page</title>
            <link href="https://fonts.googleapis.com/css?family=Niramit" rel="stylesheet" />
        </Head>
        <div className="s-row" style={{ minHeight: 70, backgroundColor: 'black', }}>
            <Navigation />
        </div>
    </div>
)

export default Header